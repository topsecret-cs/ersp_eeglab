# ERSP_EEGLAB

1. download EEGLAB version [eeglab2019_0](https://sccn.ucsd.edu/eeglab/index.php)

2. create eeglab .set file from MATLAB file
    > getdata_allphase(subjs, types)

3. plot and save figure
    > plot_fig(subjs, types)