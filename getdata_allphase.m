function [sit, stand] = getdata_allphase(subjs, types)

% subjs e.g. {'S04', 'S05' , ... }
% types e.g {'MI', 'MRCP' }
% [sit, stand] = getdata_allphase({'...'}, {'...'});
% getdata_allphase({'S04', 'S05' ,'S06', 'S07','S08','S09','S10','S11' }, {'MI'});
eeglab;

for s = subjs
    subj = s{1}
    for t = types
        type = t{1}
        if strcmp(type,'MI')
            mat_path = '../MATLAB_array/MI_allphase_matfile_1-40/'
            set_path = 'MI_set_1-40/'
            f_name = '_during_'
        elseif strcmp(type,'MRCP')
            mat_path = '../MATLAB_array/MRCP_allphase_matfile_0.1-40/'
            set_path = 'MRCP_set_0.1-40/'
            f_name = '_during_'
        end
        % make set_path directory if it doesn't exist
        if ~exist(set_path, 'dir')
            mkdir(set_path)
        end

        % create Matlab array from .mat files
        mat_sit = load(strcat(mat_path,subj,f_name,'sitting.mat'));
        sit = mat_sit.data([1:11],:);

        mat_stand = load(strcat(mat_path,subj,f_name,'stand.mat'));
        stand = mat_stand.data([1:11],:);


        chanlocs = 'eeg_chan11.locs';
        tasks = {{'sit',sit}, {'stand',stand}};

        for task = tasks
            
            eeg_data = task{1}{1,2};
            setname = strcat(subj,f_name,task{1}{1,1})
            
            % import Matlab array to EEG data  
            [EEG, com] = pop_importdata('dataformat', 'array', 'data', eeg_data, 'setname', setname, ...
            'srate', 250, 'pnts', 3500, 'xmin', -1, 'nbchan', 11, 'chanlocs', chanlocs);

            % save data EEG data to .set
            save_name = strcat(setname,'.set')
            [EEG, com] = pop_saveset(EEG , 'filename', save_name, 'filepath', set_path);
        end
    end
end