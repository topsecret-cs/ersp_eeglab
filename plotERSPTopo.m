function [ersp,ersp_sig] = plotERSPTopo(dirname,filename, subj, dest, erspmax, freqs, cycles, tlimits)



eegchannels = 1:11;

channels = {'FCz', 'C3', 'Cz', 'C4', 'CP3', 'CPz', 'CP4', 'P3', 'Pz', 'P4', 'POz'};

% cycles = [3 0.5];

% freqs = [4 40];

% baseline = [-1000 0];

% alpha = 0.05;

mine = -6;

maxe = 6;

% erspmax = 7;

eeg = pop_loadset(filename,dirname);

% make dest directory if it doesn't exist
folder = strcat(dest,subj,'/')
if ~exist(folder, 'dir')
    mkdir(folder)
end

close all;

for i=eegchannels

    tmpsig = eeg.data(i,:,:);

	tmpsig = tmpsig(:);

	triallength = size(eeg.data,2);

	close all;

	fprintf('PROCESSING CHANNEL #%2.0f\n' ,i);
% 
%       [ersp(:,:,i),itc,powbase,times,freqs,erspboot,itcboot] = ...,
%           newtimef(tmpsig,triallength,tlimits,eeg.srate,cycles, ...,
%           'baseline',baseline, 'alpha', alpha, 'freqs', freqs,'padratio',1,...,
%           'plotersp','on','plotitc','off','timesout',400, ...,
%           'title',eeg.chanlocs(i).labels,'erspmax',erspmax);
%      
%      

    % [ersp(:,:,i),itc,powbase,times,freqs,erspboot,itcboot] = ...,
    % newtimef(tmpsig,triallength,tlimits,eeg.srate,cycles, ...,
    % 'baseline',baseline,'freqs', freqs,'padratio',1,...,
    % 'plotersp','on','plotitc','off','timesout',400, ...,
    % 'title',eeg.chanlocs(i).labels,'erspmax',erspmax);

    [ersp(:,:,i),itc,powbase,times,freqs,erspboot,itcboot] = ...,
    newtimef(tmpsig,triallength,tlimits,eeg.srate,cycles, ...,
    'freqs', freqs,'padratio',1,...,
    'plotersp','on','plotitc','off','timesout',400, ...,
    'title',eeg.chanlocs(i).labels,'erspmax',erspmax);

%        
    figname = extractBefore(filename,'.set');
    set(gcf, 'PaperPosition', [0 0 10 5]); %x_width=10cm y_width=15cm
	saveas(gcf, strcat(folder ,figname,'_',num2str(i,'%d'),'_',channels{i},'.jpg'));

%      erspimage{i} = getframe(gcf,[116 80 347 308]);
        
       

%      for i1=1:size(ersp,1);
% 
%          for i2=1:size(ersp,2);
% 
%              if (ersp(i1,i2,i)>erspboot(i1,1)) & (ersp(i1,i2,i)<erspboot(i1,2));
% 
%                  ersp_sig(i1,i2,i)=0;
% 
%              else
% 
%                  ersp_sig(i1,i2,i)=ersp(i1,i2,i);
% 
%              end
% 
%          end
% 
%      end



end